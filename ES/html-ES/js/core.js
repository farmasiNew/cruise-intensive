$("#lang-wrapper .active").bind("click", function (e) {
  e.preventDefault()
});
function accBG() {
  $('#farmasifaq .card-header').each(function(){
    if($(this).find('.btn').hasClass('collapsed')){
      $(this).removeClass('darker');
    }else{
      $(this).addClass('darker');
    }
  });
}
$(window).on('load', function () {
  //lazyload();
  $('.lazyload').each(function () {
    $(this).attr('src', $(this).attr('data-src'))
    $(this).attr('srcset', $(this).attr('data-srcset'))
  });
  accBG();
});
$('#farmasifaq .card-header .btn').on('click', function(){
  $('.darker').removeClass('darker');
  //$(this).parent().parent().addClass('darker');
  setTimeout(function(){
    accBG();
  }, 100);
})
$.fn.isInViewport = function () {
  var elementTop = $(this).offset().top;
  var elementBottom = elementTop + $(this).outerHeight();

  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();

  return elementBottom > viewportTop && elementTop < viewportBottom;
};